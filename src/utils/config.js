export const serverUrl = 'http://localhost:5000';
// export const serverUrl = 'https://thetea-api.herokuapp.com';
export const serverLocal = 'http://localhost:5000';
export const configHeader = {
    headers: {
        'Authorization': localStorage.getItem('tokens')
    }
};
    